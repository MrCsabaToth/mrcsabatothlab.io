require "fastimage"

module Kramdown
  module Converter
    class Amp < Html
      def convert_img(el, indent)
        img_src = el.attr["src"]
        img_src = img_src.slice(1..-1) if img_src.start_with?("/")

        if img_src.end_with?(".jpg") || img_src.end_with?(".jpeg") || img_src.end_with?(".png")
          src_parts = img_src.split(".")
          name_last = src_parts[-2]
          extension = src_parts[-1]
          src_parts[-2] = name_last + "_xl"
          file_xl = src_parts.join(".")
          src_xl = "/" + file_xl
          w_xl, h_xl = FastImage.size(file_xl)
          src_parts[-1] = "webp"
          webp_xl = "/" + src_parts.join(".")

          src_parts[-1] = extension
          src_parts[-2] = name_last + "_lg"
          file_lg = src_parts.join(".")
          src_lg = "/" + file_lg
          w_lg, h_lg = FastImage.size(file_lg)
          src_parts[-1] = "webp"
          webp_lg = "/" + src_parts.join(".")

          src_parts[-1] = extension
          src_parts[-2] = name_last + "_md"
          file_md = src_parts.join(".")
          src_md = "/" + file_md
          w_md, h_md = FastImage.size(file_md)
          src_parts[-1] = "webp"
          webp_md = "/" + src_parts.join(".")

          src_parts[-1] = extension
          src_parts[-2] = name_last + "_sm"
          file_sm = src_parts.join(".")
          src_sm = "/" + file_sm
          w_sm, h_sm = FastImage.size(file_sm)
          src_parts[-1] = "webp"
          webp_sm = "/" + src_parts.join(".")
          src_parts[-1] = extension

          img_alt = el.attr['alt']
          "<amp-img class='border border-secondary' layout='responsive' src='#{webp_xl}' srcset='#{webp_xl} #{w_xl}w, #{webp_lg} #{w_lg}w, #{webp_md} #{w_md}w, #{webp_sm} #{w_sm}w' alt='#{img_alt}' width='#{w_xl}' height='#{h_xl}'><amp-img class='border border-secondary' fallback layout='responsive' src='#{src_xl}' srcset='#{src_xl} #{w_xl}w, #{src_lg} #{w_lg}w, #{src_md} #{w_md}w, #{src_sm} #{w_sm}w' alt='#{img_alt}' width='#{w_xl}' height='#{h_xl}'></amp-img></amp-img>"
        else
          w, h = FastImage.size(img_src)
          "<amp-img class='border border-secondary' layout='responsive' #{html_attributes(el.attr)} width='#{w}' height='#{h}'></amp-img>"
        end
      end
    end
  end
end

class Jekyll::Converters::Markdown::AmpImageProcessor < Jekyll::Converters::Markdown::KramdownParser

  def initialize(config)
    super(config)
  end

  def convert(content)
    document = Kramdown::Document.new(content, @config)
    html_output = document.to_amp
    if @config["show_warnings"]
      document.warnings.each do |warning|
        Jekyll.logger.warn "Kramdown warning:", warning
      end
    end
    html_output
  end
end
