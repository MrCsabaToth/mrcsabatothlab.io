#!/bin/bash

# Check if an input image file is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <input_image_file>"
    exit 1
fi

input_file="$1"

# Get the base filename without extension
filename=$(basename "$input_file")
extension="${filename##*.}"
filename="${filename%.*}"

# Define the sizes and formats
sizes=(1070 890 650 470)

# Loop through sizes and formats
for size in "${sizes[@]}"
do
    case $size in
        1070) postfix="xl" ;;
        890) postfix="lg" ;;
        650) postfix="md" ;;
        470) postfix="sm" ;;
    esac

    output_file="${filename}_${postfix}"
    convert "$input_file" -resize "${size}x" -quality 70 -strip "$output_file".jpg
    convert "$input_file" -resize "${size}x" -quality 80 -strip "$output_file".webp
done
