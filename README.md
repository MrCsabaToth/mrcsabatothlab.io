# csaba.page

Custom Jekyll website, now using AMP.

## Prerequisites

To install this project, you'll need Ruby and [Jekyll](http://jekyllrb.com/).

## Local Installation

1. Clone this repo, or download it into a directory of your choice.
2. Deploy to source control.

## Usage

**Development mode**

`jekyll serve` or `bundle exec jekyll serve`

## Tests

If you want to run the tests on your local machine please install `gem install html-proofer`. And then run the tests using
```shell
$ htmlproofer ./_site
```

[license-image]: https://img.shields.io/badge/license-ISC-blue.svg
[license-url]: https://github.com/MrCsabaToth/csaba.page/blob/master/LICENSE
[travis-image]: https://travis-ci.org/MrCsabaToth/csaba.page.svg?branch=master
[travis-url]: https://travis-ci.org/MrCsabaToth/csaba.page
[dependencyci-image]: https://dependencyci.com/github/MrCsabaToth/csaba.page/badge
[dependencyci-url]: https://dependencyci.com/github/MrCsabaToth/csaba.page
