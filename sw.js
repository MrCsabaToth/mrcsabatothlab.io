// sw.js

// AMP
importScripts('https://cdn.ampproject.org/sw/amp-sw.js');
AMP_SW.init();

// set names for both precache & runtime cache
workbox.core.setCacheNameDetails({
    prefix: 'csaba.page',
    suffix: 'v1.6.33',
    precache: 'precache',
    runtime: 'runtime-cache'
});

// let Service Worker take control of pages ASAP
workbox.skipWaiting();
workbox.clientsClaim();

// default to `networkFirst` strategy
workbox.routing.setDefaultHandler(workbox.strategies.networkFirst());

// let Workbox handle our precache list
// NOTE: This will be populated by jekyll-workbox-plugin.
workbox.precaching.precacheAndRoute([]);

workbox.routing.registerRoute(
  /images/,
  workbox.strategies.staleWhileRevalidate()
);

workbox.routing.registerRoute(
  /^https?:\/\/fonts\.gstatic\.com/,
  workbox.strategies.staleWhileRevalidate()
);

workbox.routing.registerRoute(
  /^https?:\/\/www\.googletagmanager\.com/,
  workbox.strategies.networkOnly(),
  'POST'
)
