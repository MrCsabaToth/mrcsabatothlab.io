---
layout: post
title: AMP website improvements
teaser: I'm using CloudFlare with AMP Real URL, GitLab Jekyll hosting, pagination and more improvements
date: 2020-05-10 12:00:00
page_id: amp-site-improvements
comments: true
category: blog
redirect_from:
  - /blog/2020/05/10/amp-site-improvements/
---
Since [my previous blog post]({% post_url blog/2020-04-30-amp-version-measurements %}) a lot of changes happened in the back-end and in the front-end of the website. Back-end changes:

* I moved from _Netlify_ + _GitHub_ hosting back-end to GitLab. The main reaosn is that _GitHub_ has an [extremely restrictive policy about supported Jekyll plugins and their versions](https://pages.github.com/versions/). So far I was playing along, but when I tried to upgrade to Jekyll 4.0 I realized it's not possible with GitHub. That's not a deal-breaker, however I'm also introducing pagination, and the standard [jekyll-paginate](https://github.com/jekyll/jekyll-paginate) plugin is not even lame, but not even actively developed any more. And that's what GitHub lists as supported. Since technically my featurettes and resume cards are blog items as well, I needed a paginator which supports category filtering. It turns out that _GitLab_ not only supports [many static website generators besides Jekyll](https://docs.gitlab.com/ee/user/project/pages/) (like _Hugo_ - this is why I originally moved to _Netlify_), but doesn't have any limits on the Gems and thor versions I use.
* So now I'm with _GitLab_. Another advantage of _GitLab_ hosting is that the build of the page and the associated CI/CD pipeline is not a black box (as kind of it is with GitHub), but [you have a full control over it](https://gitlab.com/MrCsabaToth/mrcsabatoth.gitlab.io/-/blob/master/.gitlab-ci.yml). Although _Netlify_'s pipeline is also not a black-box, I like that _GitLab_ offers the repository, the CI/CD build, and the hosting at the same place.
* _GitLab_ does not support _app_, _dev_, and _page_ TLDs (Top Level Domains) yet, so my domain remained with Google Domains as a registrar. But _CloudFlare_ manages my DNS entries for my custom domain.
* I use _CloudFlare_ for my website and I use the [AMP Real URL](https://www.cloudflare.com/website-optimization/amp-real-url/) feature along with [Rocket Loader](https://support.cloudflare.com/hc/en-us/articles/200168056-What-does-Rocket-Loader-do-). Turning on these features was a simple button click. I will have to revisit the latter one to ensure it doens't do any harm, since AMP is all about async and deferral anyway.

Front-end changes:
* I provided scaled down versions of the two carousel banners in the size of 1140, 1024, 768, and 412. These are common resolutions. I also confined the carousel to not be full width, but would remain in the content column, so on large screens it would not be as large. The banner load takes some time, so I have some incentive to maybe drop it all together or move it to the about page.
* I decided to move from _IBM Plex Sans_ to _Roboto_. My primary goal was and always be as better readability as possible. My candidates were _IBM Plex Sans_, _Ventura_, or _Roboto_. _Roboto_ is extremely well readable and also so widely used, that possibly the viewer client already has it before the browser will get it from my hosting. For example it's the default font for Chrome OS and Android. I still use 300, 400, 500, and 700 weight versions and I only have standard Latin character set to minimize the size. I don't think I'll use any Latin Extended characters. I still host my own, because [what Google pulls in has too much unnecessary fluff](https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap). I also added a _ttf_ hosted font file set.
* With the _GitLab_ hosting it was possible to use [jekyll-paginate-v2](https://github.com/sverrirs/jekyll-paginate-v2). This offers category based pre-filtering, [pagination trail](https://www.diversit.eu/post/2017/09/04/pagination-plugin.html), and other features to name a few. I always like to [contribute back](https://github.com/sverrirs/jekyll-paginate-v2/pull/191). The pagination controls are _amp-sidebar_ instances. They needed some slight color tuning changes.
* I redesigned the footer menu: the items are now icons and a _StackOverflow_ (_StackExchange_) flair was added. That flair disappears on extra small screens and a _StackOverflow_ icon takes its place. That trick required some pull-in from the Bootstrap4 CSS files.

The journey continues and I'll perform some measurements soon. Remember that the target is a 100 / 100 / 100 / 100 score.
