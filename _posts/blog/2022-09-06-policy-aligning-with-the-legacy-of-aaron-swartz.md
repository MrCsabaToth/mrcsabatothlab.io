---
layout: post
title: Policy aligning with the legacy of Aaron Swartz
teaser: A recent White House announcement was something of a surprise, and it aligns with certain activism of Aaron Swartz
date: 2022-09-06 12:00:00
page_id: policy-aligning-with-the-legacy-of-aaron-swartz
comments: true
category: blog
redirect_from:
  - /blog/in-memory-of-aaron-swartz.html
---
My career several times intertwined with academic research, I was co-author of several publications and managed to produce a few as a first author as well. It always amazed me how much effort goes into publishing a paper. A huge effort is to write the first version (already going through several internal revisions) submitted to peer review, but the real hard work is to satisfy all reviewers. The latter step sometimes results in virtually changing and revising almost every line of a publication, almost rewriting it. The tediousness of this process promotes higher quality. (Nothing is perfect though, but science always thrives for quality.)

At the same time during research one often reads other publications and unless someone is part of an academic institute that can be costly. Journals often release publications for money. The peer review process for example requires a lot of time not just from the publication authors but the peer reviewers as well. The reviewers could spend many hours meticulously going through a transcript so they can make suggestions for improvements. Interestingly however the peer reviewers do not get paid for that (as far as I saw) and they perform their services pro-bono in a volunteer fashion.

So the peer reviewers don't get paid, and most of the time the research projects are funded by grants backed by essentially taxpayer money. Various science organizations such as NSF or NIH budget are heavily governmental funded. So the question arises: if the whole research was covered by taxpayer money and the peer reviewers aren't get paid either then what is up with all of those paywalls? If you are a member of a university or academic organization you may not see paywalls because your institute pays tens of thousands of dollars for subscriptions to journals.

[Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz) was a brilliant computer programmer and progressive activist, and during his time at MIT he found it unreasonable for all the taxpayer-funded documents to not be available for free. When talking about scientific publications their results could enhance humankind and speed up more research. Aaron ended up using his skills to download documents - which should have been freely available via such transitive thinking - and made them available for everyone to access.

His actions drew the attention of even the FBI and secret services; he was slapped with a gigantic lawsuit in 2011. Sometimes multiple murderers do not get an outlook of 50 years of imprisonment and 1 million dollars of fine. Unfortunately, the mounting pressure of the lawsuit proved to be too great and Aaron committed suicide in 2013. One documentary worth watching is ["The Internet's Own Boy: The Story of Aaron Swartz"](https://www.youtube.com/watch?v=RortOTy4sHs) with a pretty high rating on IMDB or Rotten Tomatoes.

The reason I am writing this post is that finally the [White House banned paywalls on taxpayer-funded research](https://www.whitehouse.gov/ostp/news-updates/2022/08/25/ostp-issues-guidance-to-make-federally-funded-research-freely-available-without-delay/). Aaron Swartz's death is a huge loss, but his activism was not in vain after all. A few thoughts from [ExtremeTech's post on the topic](https://www.extremetech.com/extreme/339162-white-house-bans-paywalls-on-taxpayer-funded-research):
* "Under the new policy, research performed with federal dollars must be made public on the same day it appears in a scientific journal."
* "For a college or university, even the bare minimum of journal subscriptions can add up to thousands of dollars a year, which is a hard sell on a limited budget."
* The publications in question "already got government funding because it was in the public interest".
