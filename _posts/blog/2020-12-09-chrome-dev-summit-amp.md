---
layout: post
title: Chrome Dev Summit and Website Changes
teaser: Updates on recent website changes and shout out to Chrome Developer Summit
date: 2020-12-09 12:00:00
page_id: chrome-dev-summit-amp
comments: true
category: blog
---
The [2020 Chrome Developer Summit](https://developer.chrome.com/devsummit/) was just last week and it inspired me to re-run some Lighthouse tests and touch up a few issues on my website. I ran the following tests multiple times:

1. [Lighthouse tests in a Chrome Extension form](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk?hl=en).
1. [Web.dev/measure](https://web.dev/measure/)
1. [https://www.webpagetest.org/](https://www.webpagetest.org/)
1. [AMP Page Experience Guide](https://amp.dev/page-experience/?url=https://csaba.page)

I touched up the following issues:
1. I corrected the bug with the blog post comments. From now the comment section is separated for each blog post. Earlier all comments appeared for all blog posts (all of them were in one realm). As a side effect of the fix the existing comments are not accessible any more. Please comment again if you feel your voice to be heard!
1. &quot;Heading elements are not in a sequentially-descending order&quot;: I modified the HTML in multiple places to adhere to the rules.
1. &quot;iframe without title&quot;: the Disqus integration's iframe was affected. I almost thought this was about the &lt;title&gt; element in the iframe, but in reality this is about the *title attribute*. Fortunately if I supply a title attribute to the *amp-iframe* element then the AMP mechanisms pass that over to the generated *iframe* element.
1. I fiddled around a lot with the Workbox plugin since the Lighthouse recently falsely claims that the page marked with the PWA Manifest's start_url is not functioning off-line. I verified later with a test on my phone that it indeed works off-line. But this error made me double check everything, adjust caching rules and at some point I even tried to switch to [jekyll-pwa-workbox](https://github.com/souldanger/jekyll-pwa-workbox), because [jekyll-workbox-plugin](https://github.com/bmeurer/jekyll-workbox-plugin) has a [bug with wildcard caching rules](https://github.com/bmeurer/jekyll-workbox-plugin/issues/5). I had to revert back to [jekyll-workbox-plugin](https://github.com/bmeurer/jekyll-workbox-plugin) because [jekyll-pwa-workbox](https://github.com/souldanger/jekyll-pwa-workbox)'s code doesn't fit seamlessly into my AMP setting.
1. One of the tools found a slightly malformed HTML anchor. Visual Studio Code's auto-complete tends to insert the closing &lt;/a&gt; tag and sometimes this causes the open-close tag hierarchy to go out-of sync due to an extra closing tag.
1. I modified all of the Manifest icons to be maskable. This required to shrink the QR code pattern on all of the icons and resave them.
1. AMP Optimizer advised to place a precache for the main AMP JavaScript library.
1. I also used the *data-hero* attribute for the about page's carousel images. Maybe I'll use them somewhere else as well.
1. Lighthouse or PageSpeed Insights [reports all AMP scripts as &quot;Unused Javascript&quot; is a known issue](https://github.com/ampproject/amphtml/issues/28638). I believe that in the future this can be overcome by using modularized AMP components in case the browser supports modularization. These types of AMP components are in beta right now.
1. I was not able to uncover which JavaScripts were reported as unused because there is some kind of a [Linux or distribution specific Lighthouse Chrome Extension bug](https://github.com/GoogleChrome/lighthouse/issues/11771).

Chrome Dev Summit revolved around privacy and security a lot. [Core Web Vitals](https://web.dev/vitals/) was also a very important topic: three major metrics are established. I'd like to take advantage of them with my website but right now I don't see how to nicely integrate [Google Analytics's Core Web Vitals, I'd expect to have an AMP component which leverages the technology](https://github.com/ampproject/amphtml/issues/31557). CloudFlare as a host also embraced [Core Web Vitals under their Browser Insights but I have to have a nice AMP way to integrate that as well](https://community.cloudflare.com/t/how-can-i-intergate-cloudflareinsightss-beacon-min-js-into-my-amp-site/229013).

I highly advise you to try [Chrome Dev Summit's Adventure Site](https://developer.chrome.com/devsummit/adventure/), it's a lot of fun to supplement the lack of face-to-face interaction of a traditional conference setup. GDG Fresno teamed up with GDG Little Rock and GDG Sonoma to deliver a Chrome Dev Summit Extended event on 16th of January 2021. Join us to listen to Googler presenters and Google Developer Group and Women Techmakers speakers.
