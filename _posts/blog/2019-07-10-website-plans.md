---
layout: post
title: Website details and evolution
teaser: How I'll keep evolving this website.
date: 2019-07-10 12:00:00
page_id: website-plans
comments: true
category: blog
redirect_from:
  - /blog/2019/07/10/website-plans/
  - /blog/2019/07/09/website-plans/
---
When I bought the _csaba.page_ domain I also decided to try _Netlify_ for hosting. It's free just like as _GitHub_ static page hosting (a.k.a. _gh-pages_) and it's also _Jekyll_ capable. The earlier version of the site was truly one page, however I'll start blogging. For the original page I used _Bootstrap 3 Native_ version which is a _jQuery-less_ Bootstrap implementation. Now that I have a Blog and About section as well I needed a menu, so I had to _bring back jQuery_. I took the chance and upgraded to Bootstrap 4 and jQuery 3.

[Website source code]({{ site.source_repository }})

Future plans involve optimizing load by slimming down as much JavaScript and CSS as possible. Upgrading to Bootstrap 4 got rid of the glyph icon fontset. Bootstrap 4 CSS however refers to Google Montserrat font. I'll see how this can be optimized. I will slim down the JavaScript content, which is currently only used for the hamburger menu and the carousel. I'm eyeing: [Materialize](https://github.com/Dogfalo/materialize), [Material UI](https://material-ui.com/), [ZURB Foundation](https://get.foundation/). 

I'm contemplating to try [Eleventy](https://www.11ty.io/) (inspired by [benediktmeurer.de](https://github.com/bmeurer/benediktmeurer.de/)) instead of _Jekyll_ along with a headless CMS API for blog posts.

I'll work through improvements the Lighthouse tests shows.

Finally, I'll keep the blog alive.
