---
layout: post
title: Preparing for Material Design transitions
teaser: I've mentioned the Material Design transition before and I'm getting closer. There are some interesting info here however.
date: 2019-07-24 12:00:00
page_id: preparing-for-mdc
comments: true
category: blog
redirect_from:
  - /blog/2019/07/24/preparing-for-mdc/
  - /blog/2019/07/23/preparing-for-mdc/
---
In my last post we optimized the images. By the way _Cloudinary_ seems to want to compress the images further and further (as I restested). I think it also never stops (just like me).

I mentioned _Material Design_ in several posts before. More specifically I mentioned _Material Design Light_ which is an implementation of the _Material Design_ principles. My main trigger for looking to this direction is that _jQuery 3.3.1_ has a moderate severity security flaw. _jQuery_ these days is frowned upon black sheep, and I don't share this hatred: when suddenly there's a new kool-aid and most of the people suddenly deny their past and exile well established practices; in turn many people start to work with VanillaJS and they don't realize that they are actually re-implementing their own pathetic broken jQuery derivative (like dozens of experienced jQuery contributors wouldn't be smarter than them...). I'd be happy to minimize the script and CSS resource sizes any time though. _jQuery_ is a dependency of Bootstrap 4, and the old version of the website used Bootstrap Native, which by definition is a Bootstrap derivative without jQuery dependency. When I upgraded to Bootstrap 4 I needed to bring jQuery back in, and the only main feature this website uses jQuery for (besides Bootstrap uses its capabilities) to provide the menu drawer on mobile form factors. That's admittedly very narrow need. So the security vulnerability wouldn't bother me much - this is a static website where it would not really matter - but it influences me to enhance the bandwidth optimization further.

I worked with _Material Design Light_ in multiple releases of the [Hoverboard](https://github.com/gdg-x/hoverboard) conference website project. You can tell if you work with _MDL_ when you use _mdl-*_ shaped CSS classes. However _MDL_ is now superseded by the [Material Components for the web](https://github.com/material-components/material-components-web). Material Design in itself wouldn't trigger me to move away from Bootstrap by the way, because there are many Bootstrap spin-offs which apply Material Design out of the box. However it shows me possibility to minimize the bandwidth. I know that not only the Material Design animations (for example the click ripple effects) but possibly even the menu drawer could be implemented by pure CSS. _Google Engineer_ teams spent man months (years?) to optimize those. So I see a potential to not have any script resource, or just extremely minimal script parts. On the CSS side _MDC_ is modular, so I have the opportunity to cut down the fluff I don't use. I have to add that this is true for bootstrap as well, Bootstrap's SCSS is modular too (not surprisingly).

What does this mean in numbers?
* Currently: jQuery 3.3.1 min (69917) + BS 4 min JS (78635) + BS 4 min CSS (146304) + carousel min CSS (130) = 294986
* The late _Material Design Lite_: JS (62491) + CSS (139816) + for carousel let's count [mdl-ext](https://github.com/leifoolsen/mdl-ext/)'s JS (9700) + CSS (1249)) = 213256
* _Material Components for the web_: JS (297455) + CSS (267528) = 564983

Now 300K vs 200K (without cherry picking SCSS and JS) would be nice reducement. MDC is over half meg if we bundle up everything. However after cherry picking I hope for more reduction than a 200K compound bundle, especially on the JavaScript front I expect very minimal code. All of this is because before I jumped head-first into [Material Components for the web](https://github.com/material-components/material-components-web) I wanted to see clearly and evaluate these alternatives. Right now I think I'll keep transitioning towards the _MDC_ and see where I can end up in the optimization front. I may &quot;cheat&quot; by leaving out the large carousels.

I also need to count with the Material Icons fontset, which I'll need to bring in and host, and that's an additional increase over the bare jQuery + Bootstrap way. I've read a [Medium article](https://medium.com/hackernoon/optimizing-a-static-site-d5ab6899f249) which talked about a technique of cherry picking individual fonts out of glyph fontsets with the help of [Fontello](http://fontello.com/) service, although Material Icons are not part of that service. I may dig deeper if this is possible for the Material Icons. That Median post also showed me other speed test websites. One of my main goals is to keep the uniqueness of the website as much as possible and don't become too similar to other material design websites during this effort. Stay tuned for more _Material Design_ progress and headless CMS posts.
