---
layout: post
title: Schwinn AC Performance Plus Support
teaser: Details of my journey to bring support for CycleBar's amazing bikes
date: 2020-12-12 12:00:00
page_id: schwinn-ac-performance-plus-support
comments: true
category: blog
---
[In this blog post]({% post_url blog/2020-11-27-ios-and-eqipment-support %}) I outlined my plans to support more fitness machines and I am happy to announce that an MVP (Minimum Viable Product) version of the [Schwinn AC Performance Plus](https://www.amazon.com/AC-Performance-Plus-Indoor-Cycle/dp/B002KV942W) is available now. This is the bike that [CycleBar spinning studios](https://www.cyclebar.com/) use all around the country. 

I must add that CycleBar's bikes have the console and the power package add-on for the bikes. Especially the console add-on is required for my support because that's the only way to harvest data from our workouts. Although my wife and I love Les Mills RPMs and Trips at home, I have to admit that I can push myself harder in a group spinning class. There's always someone who takes the knowingly out-of-tune bike which measures 33% more power than anyone and it's a challenge to come out as a #1 in the final CycleBar Performance report.

So when you push yourself to the brink of 300W+ average power you do not want to lose that workout data. Yes, my SUUNTO can measure my heart rate but that's way less than a bicycle can provide: cadence, power, speed, distance. The mentioned add-on above provides the [MPower Echelon2 console](https://www.amazon.com/Schwinn-MPower-Echelon2-Console-Upgrade/dp/B074TK4NQ2). However it only supports ANT+ protocols and that's not compatible with Bluetooth and therefore with your phone or tablet (* only a very few and select phones are ANT+ capable). So my first plan was to [use a bridge](https://www.youtube.com/watch?v=KcLjXYrtZFQ) called the [C.A.B.L.E. (Connect ANT+ to BLE)](http://store.npe-inc.com/cable-connect-ant-to-ble/) by North Pole Engineering. That magical gadget is able to consume the ANT+ services, transform them and expose them as Bluetooth Low Energy standard GATT characteristics I could consume. Hats off to the engineers, I ordered and tested the device and I realized several things:

1. The configuration application of that device is iOS only. So if any user is [part of the 75%+ majority of the mobile market](https://www.statista.com/statistics/272698/global-market-share-held-by-mobile-operating-systems-since-2009/) they'd need to get an iOS devices for a C.A.B.L.E.. We all know that iOS devices cost a premium over cheaper Android counterparts.
1. The configuration process is sometimes not easy. Although the process is simple compared to how complicated are the mechanics under the hood, but [I may not expect non tech savvy-end-user to go through these hoops](https://www.youtube.com/watch?v=KcLjXYrtZFQ).
1. In a spinning studio setting I often get different bikes class to class. With this gadget I'd need to repeat the configuration process every time I'm on a different bike.
1. If I want to measure my heart rate I cannot pair my heart rate sensor with the bike's Echelon2 console but I'd have to pair it with the CABLE device as an extra device adding to the configuration complexity.
1. Sometimes it takes significant time to get the configuration right and saved. The Util application immediately reconnects and sometimes I cannot interrupt the process. This is not a problem if you need to do this one time for your devices (or if you have a couple of setups you can save configurations), but a spinning studio has 30+ bikes.
1. The CABLE device is a $50 expense plus shipping costs. If I'd go this route my app's users would need to purchase their own.
1. Last but not least: I was not able to get a speed or distance reading. This one is definitely a deal breaker. According to CABLE support this could be because of the firmware version of the Echelon2 console, however I'm not in the position (nor I want) to fiddle with the consoles in a spinning studio.

Fortunately at the same time I discovered that there's a way to save the workout in a CSV format onto a USB thumb drive. The video referenced in [my step-by-step guide shows how to do that](https://trackmyindoorworkout.github.io/2020/09/23/schwinn-ac-performance-plus-support.html). There are just a few downsides and some upsides of this route:

1. It seems to record a reading (power, heart rate, cadence, distance) only every three seconds.
1. The Track My Indoor Workout application still won't be able to directly measure the workout but the workout will have to be imported.
1. This alternative is less cumbersome (believe me) than the CABLE device one. (You need to connect your thumb drive before class and long press a button during cool down).
1. Much cheaper than the CABLE device: a USB thumb drive is a couple of peanuts while the CABLE is $50 + shipping.
1. The CSV needs to be off-loaded from the thumb drive and uploaded to a file share so it can be imported into the Track My Indoor Workout application.
1. The CSV doesn't contain speed data. It contains distance data but that's not precise enough (especially with the 3 second recording interval) to compute speed, so I'll need to estimate speed from the power output.

As you may guessed I decided to go with this USB thumb drive method and developed an algorithm which is able to consume the proprietary MPower CSV file, interpolate extra measurement points to increase the interval frequency to twice per second and after that you can upload that to Strava just like it was a regularly measured activity. I'm just about to push an update which adds a progress bar for the import, increases the interpolation frequency and support for common file shares instead of RAW URLs for the CSV files.

I must add that CABLE is a pretty smart gadget but this specific use-case is not a good fit for it.
