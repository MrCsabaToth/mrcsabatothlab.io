---
layout: post
title: DevFest on demand 2019
teaser: 6 out of 15 recorded talks of Valley DevFest 2019 were selected into Google's 2019 edition of DevFest on demand
date: 2020-01-24 12:00:00
page_id: devfest-on-demand
comments: true
category: blog
redirect_from:
  - /blog/2020/01/24/devfest-on-demand/
---
[Valley DevFest 2019](https://valleydevfest.com/sessions) was a great conference in my opinion. We hired _GMAC_ to record 15 of our sessions. This was Andrew's idea and it turned out to be an excellent one (although it might have seemed expensive from some angles, but it was well worth it in the end). The video service included editing and each camera had a videographer who were actively zooming to the presenter or the demonstrations. Overall 15 sessions were recorded plus the opening and closing keynotes (see a full list later).

Later in 2019 I nominated all of [Valley DevFest 2019](https://valleydevfest.com/sessions)'s talks and completed submission to the [DevFest on demand 2019 conference](https://devfest.withgoogle.com/ondemand) which is an on-line edition of DevFests. It contains a collection of the best talks out of all the several hundreds of DevFests occured all around the world judged by Google. Good news is that 6 of our talks got featured! You can spot them easily if you filter by North America region on the [DevFest on demand 2019 website](https://devfest.withgoogle.com/ondemand), but I'll list them all here for completeness:

* [Rob Aguilera: Frontend architecture: Decoupling apps from frameworks](https://devfest.withgoogle.com/events/frontendarchitecture)
* [Andres Lopez: Hacking life with BLE](https://devfest.withgoogle.com/events/hackinglife-ble)
* [Rusty Robison: Demystifying layout with CSS Grid](https://devfest.withgoogle.com/events/cssmodule-flexiblelayout)
* [Derek Payton: Embedded programming made easy: Getting comfy with MicroPython](https://devfest.withgoogle.com/events/micropython)
* [Bianca Toledo: Breaking into User Experience Design](https://devfest.withgoogle.com/events/breaking-into-uxdesign)
* [Jason Cooksey: Doing cool stuff with Arduino](https://devfest.withgoogle.com/events/arduino)

Here is the list of [all DevFest on demand talks, from all regions, all years](https://www.youtube.com/playlist?list=PL1blf2lcaPMRmfg8wdQ_XFHmNuBObiPTL).

Here is the list of all [Valley DevFest 2019](https://valleydevfest.com/sessions) recorded talks:

1. [Rio Waller: Opening Keynote](https://www.youtube.com/watch?v=w11FUS7RytY&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=1)
2. [Holly Sowles & Laura A. Huisinga: Intelligent Interior](https://www.youtube.com/watch?v=HzyBv2QnMp8&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=2)
3. [Rob Aguilera: Frontend Architecture](https://www.youtube.com/watch?v=iOJa8yLT-bY&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=3)
4. [Bianca Toledo: Breaking into User Experience Design](https://www.youtube.com/watch?v=ufddWj-4cJw&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=4)
5. [Jason Cooksey: Practical User Experience Strategies](https://www.youtube.com/watch?v=rEypFr3Zp_E&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=5)
6. [Derek Payton: Getting Comfy with MicroPython](https://www.youtube.com/watch?v=_G0vdJreW9U&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=6)
7. [Garrett Temple: Raspberry Pi Computing](https://www.youtube.com/watch?v=l-Ue6Cnl3D0&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=7)
8. [Jason Cooksey: Doing Cool Stuff with Arduino](https://www.youtube.com/watch?v=m3_CM14OkEk&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=8)
9. [Andres Lopez: Hacking Life with BLE](https://www.youtube.com/watch?v=wUieIq1CLSA&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=9)
10. [Rio Waller: Machine Learning Workflow](https://www.youtube.com/watch?v=_8JNPDKvgLM&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=10)
11. [Kevin Moore: Intro to Android](https://www.youtube.com/watch?v=xMeHZnLgazU&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=11)
12. [Steven Hollingsworth: Operations, security, company culture, technical debt, and you!](https://www.youtube.com/watch?v=Y1w93Gk9ouk&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=12)
13. [Csaba Toth: Git workflows](https://www.youtube.com/watch?v=l58oiQ6b9e8&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=13)
14. [Anna Ingels: Build Your Own Brand](https://www.youtube.com/watch?v=uv2eoExCdv8&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=14)
15. [Rusty Robison: Demystifying Layout with CSS Grid](https://www.youtube.com/watch?v=Wo4AdhAzkfo&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=15)
16. [Mia Kay Bentzien: Bring your Designs to Life : From 3D Sculpting to 3D Printing](https://www.youtube.com/watch?v=Lg-nL8OlFac&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=16)
17. [Andrew Runner: Closing Keynote](https://www.youtube.com/watch?v=nsnoI2brsz4&list=PLfLYzWZuIvXIgmR6oCaYYA-Ez8agOrHXw&index=17)

I'm super proud of all of our speakers (not just the recorded sessions), and especially good job of those who made it to the on demand selection.
