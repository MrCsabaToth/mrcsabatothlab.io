---
layout: post
title: More Sceneform AR physics experiment
teaser: I added a new scenario to my Sceneform AR Physics app and fixed some issues
date: 2020-07-19 12:00:00
page_id: ar-physics-progress
comments: true
category: blog
---
I added a new experiment scenario to [my AR Physics app](https://play.google.com/store/apps/details?id=dev.csaba.arphysics): I call it the collision box. I outlined this as a plan in [my previous AR Physics blog post]({% post_url blog/2020-07-04-sceneform-ar-physics %}). So right now the start screen of the app presents a choice between the plank tower scenario vs this new collision box scenario.

So in the collision box example I evenly set up wooden planks standing up in a matrix pattern in a rectangular 1 meter by 1 meter box and place the red cylinder the user can yank around at the center. Right now this scenario still experiences an explosion effect, so the planks will fall and jitter. But after or during the transition to an equilibrium phase the user can drag the cylinder around which stirs the planks which are confined within the invisible collision box walls.

I got it right that the cylinder needed to be a [&quot;Kinematic object&quot;](https://pybullet.org/Bullet/phpBB3/viewtopic.php?t=7195) so the physics engine would not be able to move it, but it'd still interact with the planks. Unfortunately [the jittering effect](https://github.com/CsabaConsulting/ARPhysics/issues/3) (along the unwanted initial jumping) is visible in this scenario as well, more so than the plank tower actually. There's another error: the physics [engine keeps resetting the cylinder position back to the center and when that happens the visual position differs from the simulated position creating a hollow cylinder at the center of the collision box](https://github.com/CsabaConsulting/ARPhysics/issues/7). To get the scenario to work to this extent still required a lot of trials. I used Sceneform's [TransformableNodes](https://github.com/CsabaConsulting/ARPhysics/blob/master/sceneformux/ux/src/main/java/com/google/ar/sceneform/ux/TransformableNode.java) to let the user move the cylinder around: the user needs to tap on the cylinder and drag it. I needed to limit the cylinder's movement to the ground plane to avoid vertical displacements.

I fixed a [limitation of the simulation preventing the planks from any rotation](https://github.com/CsabaConsulting/ARPhysics/issues/5): I have to explicitly call [calculateLocalInertia](https://github.com/CsabaConsulting/ARPhysics/blob/b3d456fc9411d57cd13d2ea5f884c9e78d80d749/app/src/main/java/dev/csaba/arphysics/engine/JBulletController.java#L199). I still need to integrate [Sceneform-Bullet](https://github.com/mahmoudgalal/SceneForm-Bullet) and [JMonkey](https://jmonkeyengine.org/) physics engine selection choices. I need to form API layers between my app's internal physics logic and the AR logic to make it extensible. I also want to prevent the jitterbug.
