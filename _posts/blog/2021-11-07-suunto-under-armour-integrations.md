---
layout: post
title: SUUNTO, Under Armour Integrations and Other Enhancements
teaser: I'd like to give update you what's new and what is coming up for Track My Indoor Workout application
date: 2021-11-07 12:00:00
page_id: suunto-under-armour-integrations
comments: true
category: blog
---
In [my COVID story]({% post_url blog/2021-05-30-a-covid-story %}) I mentioned that I'm craving to offer SUUNTO, Under Armour, and Training Peaks workout upload support. I'm happy to announce that I released a new build (build 83) on the open beta channel which has the [Under Armour](https://account.underarmour.com/en-us) (a.k.a. [MapMyFitness](https://www.mapmyfitness.com/), [MapMyRun](https://www.mapmyrun.com/), [MapMyRide](https://www.mapmyride.com/)) and the [SUUNTO integrations](https://www.suunto.com/) enabled ([the portal is here](https://sports-tracker.com/)). The portal integration plus upload user experience is redesigned: now the integrations can be managed from the settings integration page along with redesigned upload bottom drawers. [Training Peaks](https://www.trainingpeaks.com/) Integrations coming as well, I already successfully uploaded workouts into their Sandbox environment so I'm just waiting for approval to have access to the production environment.

These integrations are somewhat similar but each of them has many differences. All of them are operating with OAuth authorization protocols, but each of them requires some custom headers here and there. [Under Armour](https://developer.underarmour.com/) requires uploading a [proprietary JSON format](https://developer.underarmour.com/docs/v71_Workout/). This is transparent for the user, it's just a button click, but I also offer the download of that JSON format if anyone is interested. The [SUUNTO integration](https://apizone.suunto.com/) does [not receive the file payload in the upload REST API request, but rather communicates an Azure file storage endpoint](https://apizone.suunto.com/how-to-workout-upload) and I need to upload there, which is an extra step compared to the other portals. Their system also could not accept the FIT files Strava and Under Armour did, so I had to blindly experiment and perturbate the files until I didn't get a processing error. [Training Peaks API was smooth](https://github.com/TrainingPeaks/PartnersAPI/wiki), they even have a separate sandbox environment.

Soon I'll update the applciation's page with the official logos of these integrations.

Unfortunately I had to remove some items from the homepage: it turned out that the Schwinn 510u support does not mean Schwinn 170 and 270 support. I could not find where I originally read that, but I'm really sorry for the users. It seems like those bikes use some proprietary protocols because other Bluetooth FTMS applications could not pair with them either. I really don't understand why companies resort to these proprietary protocols. The FTMS standard is well defined and not a rocket science and everyone would win if those were implemented.

[In the changelog](https://trackmyindoorworkout.github.io/changelog/) I also listed a few notable issues which can enhance application stability, bluetooth connection and file upload lag. I'll write another blog post when Training Peaks will finally be released, I'll also roll out a production release. And I'll implement some user-requested UX extras and I'm planning the HRV support too. Stay tuned.
