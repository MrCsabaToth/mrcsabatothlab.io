---
layout: post
title: AndroidX Preference<wbr>Fragment<wbr>Compat scaffolded Shared<wbr>Preferences encrypted with Jetpack Security
teaser: How to encrypt Shared<wbr>Preferences with JetSec along with Preference<wbr>Fragment<wbr>Compat XML scaffolded UI in Kotlin
date: 2020-08-16 12:00:00
page_id: jetpack-security-shared-preferences
comments: true
category: blog
has_code: true
---
Let me start by disadvising anybody from developing their own encryption algorithm, encryption protocol, or encryption schemes while trying to increase security of an application. Always use well known encryption libraries and such best practice protocols and techniques wich are advised by experts. Otherwise you can easily make a tiny mistake which could compromise your whole scheme.

This is why I'm so happy that [Android Jetpack has a Security feature set](https://developer.android.com/jetpack/androidx/releases/security) which offers encryption / decryption of SharedPreferences and file streams. The fact that it's developed by Google engineers gives a peace of mind that it'll pass a high standard of quality and since it's backed by Google it is safe to rely on it. The default advised usage uses latest algorithms and modes like [AES256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard), [GCM](https://en.wikipedia.org/wiki/Galois/Counter_Mode) and [AES-GCM-SIV](https://en.wikipedia.org/wiki/AES-GCM-SIV). Complex scenarios can be configured using [KeyGenParameterSpec](https://developer.android.com/reference/android/security/keystore/KeyGenParameterSpec), key authentication expiration can be specified and JetSec is also capable of using [Titan security module](https://www.blog.google/products/pixel/titan-m-makes-pixel-3-our-most-secure-phone-yet/) which elevates the security to hardware token level (Titan basically integrates the hardware security token into the phone).

Be careful when reading blogs about JetSec because it's still in alpha phase so the API can still change. Good example for that is the _MasterKey_: _MasterKeys_ (plural) is deprecated now, so if you read stuff like _MasterKeys.getOrCreate_ that's an older alpha version. You'd want to look for _MasterKey.Builder_. This blog post won't talk about file encryption, I want to focus on SharedPreferences encryption and specifically Kotlin - which is now the primary first class over Java citizen in the Android ecosystem. Here is a code snippet how you can easily obtain an encrypted _SharedPreferences_ instance:

{% highlight Kotlin %}
val masterKey = MasterKey.Builder(applicationContext, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
  .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
  .build()

val preferences = EncryptedSharedPreferences.create(
  applicationContext,
  SHARED_PREFERENCES_NAME,
  masterKey,
  EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
  EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
)
{% endhighlight %}

Once you have that it can be used just as if it would be a regular _SharedPreferences_. Once you've changed your code the saved cleartext data won't be compatible with the new scheme - this is not a surprise. I have to talk about _PreferenceFragment<wbr>Compat_ though: another basic thumb rule (besides do not write your own encryption) is to avoid plumbing code by all means. Less plumbing code means less maintenance and smaller bug surface area. Android Jetpack provides a really nice declarative way of defining your Preferences UI. That does all the weight lifting for you but it works with a non encrypted _SharedPreferences_ by default. Even if you take a peek at the decompiled classes you may not see how JetSec encryption can be introduced. Here is a five step guide where only the last two steps have encryption related extras.

0. Add _implementation &quot;androidx.security:security-crypto-ktx:1.1.0-alpha02&quot;_ for JetSec to you gradle.build file.
1. You need to add _implementation &quot;androidx.preference:preference:1.1.1&quot;_ to you gradle.build file for _PreferenceFragmentCompat_ and additional Jetpack preferences helper functionality.
2. Define your preferences UI in a declarative way in _res/xml/preferences.xml_ file.
3. Add a small _SettingsActivity_ stub.
4. Add a small _SettingsFragment_ stub.
5. Supply the _EncryptedPreferenceDataStore_ which provides the encrypted provider for preferences data.

Here is a snippet of the _res/xml/preferences.xml_:

{% highlight XML %}
<PreferenceScreen
  xmlns:android="http://schemas.android.com/apk/res/android"
  xmlns:app="http://schemas.android.com/apk/res-auto"
  xmlns:tools="http://schemas.android.com/tools"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  tools:context=".ui.SettingsFragment">

  <EditTextPreference
    app:key="project_id"
    app:title="@string/project_id_title"
    app:summary="@string/project_id_help"
    android:icon="@drawable/ic_one"/>

  <EditTextPreference
    app:key="application_id"
    app:title="@string/application_id_title"
    app:summary="@string/application_id_help"
    android:icon="@drawable/ic_two"/>

  <!-- ... -->

</PreferenceScreen>
{% endhighlight %}

_SettingsActivity.kt_:

{% highlight Kotlin %}
package your.package.path.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import your.package.path.R

class SettingsActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_settings)
    supportFragmentManager
      .beginTransaction()
      .replace(R.id.settings_container, SettingsFragment())
      .commit()
  }
}
{% endhighlight %}

_SettingsFragment.kt_:

{% highlight Kotlin %}
package your.package.path.ui

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import your.package.path.data.EncryptedPreferenceDataStore
import your.package.path.R

class SettingsFragment : PreferenceFragmentCompat() {
  override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
    // The following instruction makes the difference between the encrypted and the normal version:
    preferenceManager.preferenceDataStore =
      EncryptedPreferenceDataStore.getInstance(requireContext())
    setPreferencesFromResource(R.xml.preferences, rootKey)
  }
}
{% endhighlight %}

The most important code in the last snippet. The only distinction between the encrypted and the non encrypted variation is the _preferenceManager.<wbr>preferenceDataStore = EncryptedPreferenceDataStore.<wbr>getInstance(requireContext())_ line. By supplying the _Preference<wbr>DataStore_ we can influence the underlying logic to use JetSec encryption. Here is the _EncryptedPreference<wbr>DataStore_:

{% highlight Kotlin %}
package your.package.path.data

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceDataStore
import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class EncryptedPreferenceDataStore private constructor(context: Context) : PreferenceDataStore() {
  companion object {
    private const val SHARED_PREFERENCES_NAME = "secret_shared_preferences"

    @Volatile private var INSTANCE: EncryptedPreferenceDataStore? = null

    fun getInstance(context: Context): EncryptedPreferenceDataStore =
      INSTANCE ?: synchronized(this) {
        INSTANCE ?: EncryptedPreferenceDataStore(context).also { INSTANCE = it }
      }
  }

  private var mSharedPreferences: SharedPreferences
  private lateinit var mContext: Context

  init {
    try {
      mContext = context
      val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

      mSharedPreferences = EncryptedSharedPreferences.create(
        context,
        SHARED_PREFERENCES_NAME,
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
      )
    } catch (e: Exception) {
      // Fallback, default mode is Context.MODE_PRIVATE!
      mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }
  }

  override fun putString(key: String, value: String?) {
    mSharedPreferences.edit().putString(key, value).apply()
  }

  override fun putStringSet(key: String, values: Set<String>?) {
    mSharedPreferences.edit().putStringSet(key, values).apply()
  }

  override fun putInt(key: String, value: Int) {
    mSharedPreferences.edit().putInt(key, value).apply()
  }

  override fun putLong(key: String, value: Long) {
    mSharedPreferences.edit().putLong(key, value).apply()
  }

  override fun putFloat(key: String, value: Float) {
    mSharedPreferences.edit().putFloat(key, value).apply()
  }

  override fun putBoolean(key: String, value: Boolean) {
    mSharedPreferences.edit().putBoolean(key, value).apply()
  }

  override fun getString(key: String, defValue: String?): String? {
    return mSharedPreferences.getString(key, defValue)
  }

  override fun getStringSet(key: String, defValues: Set<String>?): Set<String>? {
    return mSharedPreferences.getStringSet(key, defValues)
  }

  override fun getInt(key: String, defValue: Int): Int {
    return mSharedPreferences.getInt(key, defValue)
  }

  override fun getLong(key: String, defValue: Long): Long {
    return mSharedPreferences.getLong(key, defValue)
  }

  override fun getFloat(key: String, defValue: Float): Float {
    return mSharedPreferences.getFloat(key, defValue)
  }

  override fun getBoolean(key: String, defValue: Boolean): Boolean {
    return mSharedPreferences.getBoolean(key, defValue)
  }
}
{% endhighlight %}

For Java version please look at [this StackOverflow post](https://stackoverflow.com/questions/58515475/is-there-a-way-to-integrate-encryptedsharedpreference-with-preferencescreen/63442567#63442567).
