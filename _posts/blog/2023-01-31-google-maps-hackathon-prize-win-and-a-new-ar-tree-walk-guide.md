---
layout: post
title: Google Maps Hackathon Prize Win and a New AR Tree Walk Guide
teaser: The Recycling Trashcan AR Map winning a hackathon prize and stepping up my AR game with another Geospatial API Tree Walk Guide AR app
date: 2023-01-31 12:00:00
page_id: google-maps-hackathon-prize-win-and-a-new-ar-tree-walk-guide
comments: true
category: blog
---
Geospatial API is a great technology that made the Recycling Trashcan AR Map possible. [An amazing news is that the project won a Google Maps hackathon prize](https://devpost.com/software/recycling-trashcan-ar-map) (Geospatial API is part of Google Maps API). So far I purchased a GoPro Max 360 cam with the prize and I used that to record some 360 content. When I visited GDG San Francisco for a lightning talk series of LLM startups ([AI SuperShowcase Show and Tell](https://gdg.community.dev/events/details/google-gdg-san-francisco-presents-ai-super-showcase-show-tell-what-youve-built-222/)) the GoPro Max stopped working so I'll need to look for another one, but that's a different story.

Fresno State students were working on redesigning the campus tree walk guide. The new guide contains indigenous or drought-tolerant species, and also some green spaces. These are all outdoor spots similar to the recycling trashcans and ideal use-case for Geospatial API. I started to work on an AR application and companion website for that and I [signed up for another hackathon to keep myself accountable for some deadlines](https://devpost.com/software/tree-walk-guide).

I could use even more Google Maps API features: besides custom pins: I number the stops and I connect them with lines on the companion website. I wanted the mobile app to appeal to younger generations so I introduced some gamification: the AR app utilizes the Google Play Games and users receive experience points (XP) and unlock achievements as they visit the tree walk stops.

On top of that, I wanted to use some EAP (Early Acces Program) features. One of these provided segmentation labeling of the camera view. I used that segmentation to identify green vegetation and tried to develop a "virtual watering" feature that yields some extra experience points for the user. Another step up compared to the Recycling Trahscan is that the Tree Walk is bi-lingual and can speak and display text in Spanish.

As part of the hackathon prize, I had the opportunity to have a one-on-one meeting with a Google Maps engineer. He was very receptive to my experiences and I told him my use case and experience, including the segmentation EAP API. This EAP feature was experimental and is removed from the [current release of the app](https://play.google.com/store/apps/details?id=dev.csaba.armap.treewalk). [Also check out the companion website which contains 360 photos of the stops as well](https://treewalks.github.io/).

The good news is that eventually, these apps could be part of some sustainability curriculum, and hopefully, I'll also be able to integrate them with the official Fresno State University content so it'll get more exposure and interest.

There are several tasks left for the future, I'd like to involve students with:
1. Just like we haven't mapped out all the recycling trashcans, the tree walk is not finalized either. The sustainability students need to meet with the arborist to clarify some uncertain spots, and we may have also walk variations for families with little kids, or elderly people.
2. The current app is Android only. I'd be happy to recruit students to rewrite the app in Unity. Geospatial API has a Unity plugin, however, it doesn't have the segmentation EAP feature. Another multi-platform venue would be to utilize Flutter with [SceneView Flutter](https://github.com/SceneView/sceneview-flutter). Since [Flutter Forward](https://flutter.dev/events/flutter-forward) we know that Flutter might not be 2D anymore and 3D would open up the possibility to even AR/VR. [Check out this Outpost Game Codelab how much and rich shaders it uses](https://codelabs.developers.google.com/codelabs/flutter-next-gen-uis). SceneView is a maintained Kotlin version of the archived Google SceneForm project. [Thomas Gorisse keeps it maintained](https://github.com/SceneView/sceneview-android) and hopefully one day the Flutter plugin will come to fruition.
3. [There are other issues](https://github.com/TreeWalks/TreeWalks.github.io/issues), like Hmong language support, drinking fountain locations, and possibly we can even integrate the recycling trashcan locations as an optional overlay.

I cannot wait to cooperate with some students.
