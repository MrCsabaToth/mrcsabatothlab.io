---
layout: post
title: AMP website cookie consent and further tuning
teaser: Cookie consent popup is introduced, improvements were made in several departments
date: 2020-05-22 12:00:00
page_id: amp-site-cookie-consent-tuning
comments: true
category: blog
has_code: true
redirect_from:
  - /blog/2020/05/22/amp-site-cookie-consent-tuning/
---
I promised improvements in my previous blog posts ([here]({% post_url blog/2020-05-10-amp-site-improvements %}), [here]({% post_url blog/2020-04-30-amp-version-measurements %})) and I want to keep you updated about the changes of the last twelve days.

* A major feature is that I introduced a Cookie consent notification. I'm using [amp-consent](https://amp.dev/documentation/components/amp-consent/) in combination with [amp-lightbox](https://amp.dev/documentation/components/amp-lightbox/). Sicne this is such a common scenario I thought it'll be a simple task. Unfortunately it was utterly painful. The _amp-consent_ component is pretty advanced, for example it is able to distinguish geological regions if needed.
    * _amp-consent_ is not capable of handling cookie categories though. Therefore all I needed was a modal dialog which listed the possible cookies my website would use.
    * No categories would mean that if someone rejects the consent, then all cookie related features would be turned off, there's no cherry picking or middle ground. In my case these features are Google Analytics and Disqus comments. This can be achieved by marking the _AMP_ elements which needs to be turned off with _data-block-on-consent_ attribute.
    * Without _AMP_ it's actually not trivial to block a Google Analytics snippet to kick in until a consents is Accepted. It'd require injecting some DOM after the consent. _AMP_ at least does that out of the box for us. But before that you'd need to get the consent form working.
    * I wanted to keep it simple and avoid hosting an own modal dialog in an off-site domain for an _iframe_ based solution. Cookie consent is so common that such task should be able to be carried out in-house (without external site).
    * I scouted out the [amp-story-consent](https://amp.dev/documentation/examples/user-consent/story_user_consent/) route so I'd be able to take advantage of the default UI the story can provide me. This alley was a failure. I realized that despite the simple code snippets I'd need story pages and bookend, which is just an overkill, just like the 81.4 KB size of the _amp-story-0.1.js_. [For a glimpse of my desperate tries click here](https://stackoverflow.com/questions/61929226/how-to-construct-amp-cookie-consent-with-the-help-of-amp-story-consent).
    * I tried to utilize [amp-user-notification](https://amp.dev/documentation/components/amp-user-notification/) along with _amp-consent_ but after lengthy trials I had to give up and abandon that route as well. The two just don't work together.
    * I had to move back to the basics and design my own modal dialog from ground zero. But the modal backdrop didn't want to work and it'd be quite ugly if users could still accidentally click behind the consent dialog. [Noone came to my help](https://stackoverflow.com/questions/61944487/how-to-get-amp-cookie-consent-modal-dialog-backdrop-working/61965611#61965611) until I accidentally came across [amp-lightbox](https://amp.dev/documentation/components/amp-lightbox/) during my desperate tries and that saved me from tearing all my hair out.
    * This was not needed in the end (I can use _&quot;consentRequired&quot;: true_ for a simple scenario), but just to show you how far I went: I even spinned out my own cloud function REST endpoints for _data-show-if-href_ of _amp-user-notification_ and the _checkConsentHref_ of _amp-consent_. I went through hell fighting with various flavors of _CORS_ related error messages until I figured out working versions:

{% highlight JavaScript %}
addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request))
})

/**
 * Respond to the request
 */
async function handleRequest(request) {
  return new Response(
    JSON.stringify({ "consentRequired": true }),
    {
      headers: {
        "Access-Control-Allow-Origin": request.headers.get("Origin"),
        "Access-Control-Allow-Credentials": "true",
        "Content-Type": "application/json"
      },
      status: 200
    }
  )
}
{% endhighlight %}

* _rel=&quot;noopener noreferrer&quot;_ was added to the _target=&quot;_blank_&quot; links to increase site security (hint was given by the webhint.io scanner).
* Removed some accidental _!important_ directives from CSS, these are not allowed by _AMP_ validation and are anti-patterns anyway.
* Some tools revealed that one of my _webp_ images was accidentally a _jpeg_. I re-encoded the resource. Also some manifest and touch icons weren't the expected dimensions. I regenerated those to the proper size from vectorial _SVG_ using _InkScape_.
* Added type attribute to some buttons.
* Ironed out some accessibility issues: specified labels and titles.
* The wide range of tools including the AMP validator and the Chrome Developer tools help to spot any inconsistency with validity, accessibility, or security.
* Since Roboto is the [default font of Android and Chrome OS](https://newbirddesign.com/best-google-fonts-to-use-on-your-website/) and it's widely used and cached I think it was a good decision to move towards it. I played around with the font hosting though to gauge speed. Not self hosting the font at all seemed to not yield any speed increase. I wanted to move to Google _CDN hosting_, but their link means an extra request. I settled with a hybrid solution: for _woff2_ I refer to Google's CDN, but I self host the _woff_ and the _ttf_ versions. This seems to be an ideal solution so far which doesn't hurt performance for default cases.
* On the hosting front-end I'm minifying the HTML content. This is done by _jekyll-minifier_ and _compress-html_.

Let's talk about the measurements:

1. [Official Lighthouse](https://web.dev/measure), [results](https://lighthouse-dot-webdotdevsite.appspot.com//lh/html?url=https%3A%2F%2Fcsaba.page): 98 / 100 / 100 / 100
2. [webpagetest.org Lighthouse](https://www.webpagetest.org/lighthouse), [results 05/14/2020](https://www.webpagetest.org/result/200515_B0_73b903350f01791b87d0e9d0ca39339d/): 96 / 100 / 100 / 100, [results 05/15/2020](https://www.webpagetest.org/result/200515_27_a71de98c5b4d9926823dc6a29804b6ab/): 94 / 100 / 100 / 100
3. [webhint.io scanner](https://webhint.io/scanner/), [results](https://webhint.io/scanner/a86e71b1-8255-4a03-bb44-45b7f8822017): 12 / 49 hints

Future plans: feature [amp-google-vrview-image](https://amp.dev/documentation/examples/components/amp-google-vrview-image/) on blog posts which include VR content (GooglePlex visits for example). Besides that the goal is to improve further and reach 100 / 100 / 100 / 100 score.
