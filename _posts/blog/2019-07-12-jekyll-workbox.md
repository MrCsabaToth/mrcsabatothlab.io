---
layout: post
title: Service workers for your PWA (Progressive Web App) website
teaser: Manifest for PWA is a no brainer, but what about service workers?
date: 2019-07-12 12:00:00
page_id: jekyll-workbox
comments: true
category: blog
has_code: true
redirect_from:
  - /blog/2019/07/12/jekyll-workbox/
  - /blog/2019/07/11/jekyll-workbox/
---
Two pillars for transforming your website into a _PWA_ (_Progressive Web App_) are the manifest and having service workers. (Besides that it's better if your website is responsive, follows _PRPL_ principles, but I won't go into these in the current blog post.) Manifest is kind of a no brainer, although requires some work. You need to identify the main theme color and main background color for your website, and generate various sizes of the favicon of your website and reference some of those from the manifest and some of them from the header meta section. There are several websites ([Example](https://tomitm.github.io/appmanifest/)) which help you to generate series of images and the corresponding manifest and HTML meta tags.

On the other hand the service workers are not as evident since your website could take several shapes and forms (for example _SPA_ / non _SPA_), sub URLs and assets. You first need to decide what caching strategies you prefer and for what part of your website: do you prefer off-line first or on-line first approach. On-line first approach could be a good idea if your website for example displays crypto currency tickers and their current values. In case of a static website with a blog section (like mine) you are good to go with an off-line first approach. Still, you need to configure which files exactly the service worker infrastructure should cache for you for off-line use and what way (expiration, etc.). You can decide the approach per resource basis, so your static website can be off-line first, while only the trade ticker part could be on-line first with an off-line fall-back.

In the past _Google_ provided libraries like _sw-precache_ to help with this configuration task and possibly cut down plumbing code related to the service worker configuration and activation mechanisms. The newest generation of _Google_'s service worker helper library is [Workbox](https://developers.google.com/web/tools/workbox/), and that takes the help to a next level and I believe we arrived to a point where the configuration is minimal and painless. Ideally you want to only specify the list of assets (possibly with wildcards) and what strategy you want to follow for each. Then the framework should take care of the rest for you.

Along that line: in case you are using any static site generator framework then there's a very good chance that your framework has a package which provides a layer on top of _Workbox_ and you can deal with it at the level of your generator framework instead of _Workbox_'s _JavaScript_ API. A common principle in software engineering is to avoid reinventing the wheel, so you can spend your time with the more meaningful tasks of your project. Unless your framework of choice is very exotic and doesn't have much community behind it yet, service worker configuration and dealing with _Workbox_ must have been done by thousands of engineers before you, and steps probably taken to make this part seamless. This means: *always* look how your selected framework supports _PWA_ and look for a layer on top of _Workbox_.

At the time of this blogpost this site currently uses Jekyll, and I saw two candidates (BTW, multiple choices are usually a good sign of thriving community): [jekyll-pwa-plugin](https://github.com/lavas-project/jekyll-pwa) or [jekyll-workbox-plugin](https://github.com/bmeurer/jekyll-workbox-plugin). Both of them seems nice, but according to [this blogpost](https://benediktmeurer.de/2018/12/06/introducing-jekyll-workbox-plugin/) I decided to go with the latter. Since Jekyll is _RoR_ (Ruby on Rails) based framework the &quot;package&quot; you need to install is a _Ruby Gem_. The configuration is simple, well readable (_yaml_ format), no fluff inside the expected *_config.yml*, and the few plumbing code placeholders. I only needed to add two _JavaScript_ entries:

In _default.html_: [source]({{ site.source_repository }}/-/blob/cd420a332c73abd85c5be4dc95e6965e2062bd5f/_layouts/default.html#L23)
{% highlight JavaScript %}
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register("{{ '/sw.js' | relative_url }}").then(function() {
      console.log("Service Worker Registered");
    });
  });
{% endhighlight %}

And a short _sw.js_ file stub ([source]({{ site.source_repository }}/-/blob/afdeefbf2b902e07b1b3af074be4d5c30ac83886/sw.js#L11))
{% highlight JavaScript %}
// let Workbox handle our precache list
// NOTE: This will be populated by jekyll-workbox-plugin.
workbox.precaching.precacheAndRoute([]);
{% endhighlight %}

And that's all!
