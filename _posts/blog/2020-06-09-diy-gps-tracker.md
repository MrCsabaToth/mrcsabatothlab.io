---
layout: post
title: Do-It-Yourself GPS Tracker and Manager
teaser: Free open source application set for tracking asset locations and managing them
date: 2020-06-09 12:00:00
page_id: diy-gps-tracker
comments: true
category: blog
---
[TL;DR: DIY GPS Tracker app bundle set](https://diygpstracker.github.io).

I'm somewhat of a paranoid engineer. For example I go great lengths to protect my valuable assets. I use two factor authentication wherever I can and mostly hardware key / Authenticator App type, and I explicitly disable SMS based 2FA. I use bluetooth based trackers ([Tile](https://www.thetileapp.com/) and [Nut](https://www.nutfind.com/)) to secure expensive laptops and cars. However bluetooth based trackers only work if an in-network device is in proximity with the protected assets.

Although desktop replacement laptops are often in the range of $2500-$4000, bluetooth trackers are small enough to easily conceal in a hard drive bay. A car can still cost a magnitude more than that, hence they deserve better protection. There are GPS trackers offered on the market, however they all lock you into their own ecosystem. On top of that the older models may rely on 2G and 3G mobile networks which are being phased out.

Since I'm a [Google Fi subscriber](https://www.theverge.com/2017/6/13/15782436/project-fi-data-only-sim-deal-wireless-lte-tmobile) I can order 4 [data SIM cards](https://support.google.com/fi/thread/912102?hl=en) without costing me any fixed monthly fees apart from the actual data costs. GPS location reporting won't generate too much data though. After researching I decided to ditch the idea of a small custom device (could have been some Arduino or Raspberry Pi based one) and simply go with a used phone. Google Fi partially supports Motorola Moto g6 and I could get a carrier independent good condition one on eBay in the $30 price range. An Android phone will allow me to develop both the tracker (will run on the tracker Moto g6 device) and the manager (will run on my personal phone) app with familiar tools in a familiar ecosystem. The Moto g6 provides me with an Android 9 which is good enough.

I wanted to make both applications open source and also release them to the Google App Store. Besides that I wanted to avoid at all cost to touch any of the sensitive location data what the potential users may log. I choose Firestore as the applications' database backend since it has very good Android support and offers off-line capability transparently out of the box. This can be useful if the protected asset goes through an area where there's no data connection: the Firebase layer will automatically gather the reported GPS locations until the connection is restored, at which point it syncs them up to the Firestore.

But as I mentioned I didn't want to store the data on my own: here comes the Do It Yourself nature of the application bundle. The user has to have some degree of technical knowledge to set up their own Firebase project with the Firestore, and they'll provision all their data there by themselves. This proved to be harder than I thought: every Firebase integration requires a Firebase project established and hard-coded. That project in our case becomes a dummy project and never receives any data; that project doesn't even have any authentication method enabled. The user's project and firestore needed to be configured at run-time, and that will act as a &quot;secondary&quot; Firestore. The app will communicate and perform operations only on this secondary project, however that is off the beaten path and everything which is like that needs extra hoops and effort to achieve.

My goal was to provide an open source solution where _you manage and possess your own data_. You could say that my solution is locked-in to Firebase, but you can export it any time. The bills you'll receive for your Firebase project will be negligible, mostly $0 but not more than 5 cents per month.

My main intent is theft protection. All the device will need is constant power. It won't be in an interactive setting though, so it's important to make sure that in case of any crash or blitz the DIYGPSTracker would be able to restart on its own without any user interaction. Hence the reason I went with the Email + Password authentication method. From a security perspective the Google authentication would be better, but in case of a restart it could be stuck with a Sign-In dialog.

You must analyze your use-case and secure it down as much as possible. It's beneficial to provide a fall-back solution in case of any failure. For each asset I created separate Google accounts representing a child (the manufacturing year of the cars as a birth year 2016 and 2017). The accounts related to the protected cars are then also managed by [Google Family Link](https://families.google.com/familylink/) system. That could also provide an extra backup: in any case the DIYGPSTracker would fail, I could still request the location of the device using the [Family Link](https://families.google.com/familylink/) manager app. It also provides a 

Please [visit DIYGPSTracker website](https://diygpstracker.github.io) for more information, links to the applications and their source codes. Feel free to test it, file any issues in the GitHub source repositories or even better: contribute with PRs.
