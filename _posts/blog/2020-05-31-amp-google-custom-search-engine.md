---
layout: post
title: Google Custom Search Engine on an AMP website
teaser: How to integrate Google powered custom search into your AMP website
date: 2020-05-31 12:00:00
page_id: amp-google-custom-search-engine
comments: true
category: blog
has_code: true
---
My friends provided some important feedback about my website and one of them was to integrate a search functionality. I've probably seen that in many websites where the search form lands on a custom Google Search page which only searches the given website. That is kind of the same as if you'd provide *site:csaba.page* search qualifier for your search phrase on google.com.

These are the steps I took, mostly inspired by [this blog post](https://gfw-blog.netlify.app/2018/05/11/cse-amp.html), [also see this StackOverflow answer](https://stackoverflow.com/a/62068893/292502):

1. Create a GCSE (Google Custom Search Engine) at the [CSE (Custom Search Engine) Console](https://cse.google.com/) as described by [Google documentation on the matter](https://developers.google.com/custom-search/docs/tutorial/creatingcse) if you haven't already done so.
2. Remember your *Search engine ID*, it looks something like *013535696182405026577:<wbr>kmxne16xdtx*. You can see it on the *Basics tab* in the CSE Console, or if you are looking at the offered JavaScript code it's the *cx variable*. Note: you won't be able to use that JavaScript code.
3. Include *amp-form* in your head section where the other AMP includes are. See Figure 1 below.
4. In the *body* add an *amp-form* where you want to position the search input and style the form as you need, see Figure 2 below.

##### Figure 1:
{% highlight HTML %}
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js" crossorigin="anonymous"></script>
{% endhighlight %}

##### Figure 2:
{% highlight HTML %}
<div>
  <form method="GET" class="search-form" action="https://www.google.com/cse" target="_top">
    <div class="search-form-inner">
      <input name="cx" type="hidden" value="013535696182405026577:kmxne16xdtx" />
      <input name="ie" type="hidden" value="UTF-8" />
      <input type="search" placeholder="Search..." name="q" required>
      <input type="submit" value="Go" class="search-button">
    </div>
  </form>
</div>
{% endhighlight %}

In my case I positioned the search form onto a new second side-bar which opens up from the right side.

Other changes to my website were numerous but less notable:

* The purple color got changed slightly to match one of the tiles.
* I discovered that the hamburger icon (HTML equiv symbol) does not come out the same on Firefox as it does with Chrome. Therefore I introduced SVG icons for the hamburger menu sign. I also needed to introduce an SVG icon for the search magnifier glass anyway.
* Since I changed my permalink structure I noticed in the Google Search Console (for webmasters) that many search phrases still refer to the old permalink pattern URL pages. I looked at two Jekyll packages: [jekyll_alias_generator](https://github.com/tsmango/jekyll_alias_generator) and [jekyll-redirect-from](https://github.com/jekyll/jekyll-redirect-from#redirect-to). The latter looked active and I added that plugin to my page and added *redirect_from* URLs to the post front-end matters.
* Restructured the header to feature the QR code logo instead of simply my name. The toolbar menu in the header is more responsive and it features more menus based on the current form factor.
* The rest of the changes were sizing and styling improvements.
