---
layout: post
title: Sceneform Good News
teaser: Romain Guy drew my attention to a maintained fork of the SceneForm repository
date: 2021-03-16 12:00:00
page_id: sceneform-good-news
comments: true
category: blog
---
In the [Sceneform Breaking Change blog post]({% post_url blog/2020-06-20-sceneform-breaking-change %}) I described that if you follow the Sceneform README's advice you would most probably need to upgrade the two source folders to AndroidX. So many developers forked the repository, some even modified the code style while I restricted myself to the strictly necessary changes so I can merge any potential changes from upstream (although Google's repository is in an archived state but I'm always optimistic).

I just wanted to share a great news: Romain Guy drew attention to a maintained repository of Sceneform, see [his Tweet: &quot;If you are looking for an up-to-date version of Sceneform that uses the latest Filament and supports both AR and non-AR use case, this might interest you:&quot;](https://twitter.com/romainguy/status/1371864003882807300) [https://github.com/ThomasGorisse/sceneform-android-sdk](https://github.com/ThomasGorisse/sceneform-android-sdk).

This may still not help to those who would [want an Android Studio plugin to convert sfa and sfb formats](https://stackoverflow.com/questions/64573442/plugin-google-sceneform-tools-beta-is-incompatible-supported-only-in-intel/64918807#64918807), but upon looking at the repository it seems to be active, and that is a really good news in my opinion. I'll transition to that repository with my Augmented Reality projects.
